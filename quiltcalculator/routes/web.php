<?php

/*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/borders', 'CalculatorController@borders');
Route::post('/borders', 'CalculatorController@bordersCalculate');

Route::get('/backing', 'CalculatorController@backing');
Route::post('/backing', 'CalculatorController@backingCalculate');

Route::get('/sashing', 'CalculatorController@sashing');

Route::get('/binding', 'CalculatorController@binding');

Route::get('/precut-yardage', 'CalculatorController@precutyardage');

Route::get('/yardage-precut', 'CalculatorController@yardageprecut');

Route::get('/pattern-scaler', 'CalculatorController@patternscaler');
