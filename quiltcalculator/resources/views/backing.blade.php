@extends('layout.app')
    <body>
<div class="container">

  <div class="row">
    <div class="col col-md-12 text-center">
      <div class="welcome">
        <h1 class="display-1" style="font-size: 5em;">Backing Calculator</h1>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col col-md-12 text-center">
      <div class="welcome-text">
        <h3 class="display-3">How much backing fabric do you need?</h3>
        <h3 style="padding-bottom: 10px;">Lets find out!</h3>
        <h4>Scroll to the bottom for results.</h4>
        <h4 style="padding-bottom: 10px;">Need something else? <a href="/">Go Back!</a></h4>
      </div>
    </div>
  </div>

  <div class="row">
    <form method="post">
      {{ csrf_field() }}
      <div class="col col-sm-12">


      <div class="form-group">
        <label for="wof">Seam Direction</label> - <small class="form-text text-muted">Backing seam top to bottom or left to righ.</small>
        <div class="radio">
          <label><input type="radio" name="seam" value="Horizontal">Horizontal</label>
        </div>
        <div class="radio">
          <label><input type="radio" name="seam" value="Vertical">Vertical</label>
        </div>
        <div class="radio">
          <label><input type="radio" name="seam" value="noseam">No Seam (108' backing only)</label>
        </div>

        <br />

        <label for="wof">Width of Fabric</label> - <small class="form-text text-muted">108 inches is pretty standard for backing.</small>
        <input type="number" class="form-control" aria-describedby="width of fabric" name="wof" value="108">

        <br />

        <label for="quiltheight">Overage</label> - <small class="form-text text-muted">For machine quilting. 4 inches is standard.</small>
        <input type="number" class="form-control" aria-describedby="quilt height" name="quiltheight" value="4">

        <br />

        <label for="quiltheight">Quilt Height</label> - <small class="form-text text-muted">From top to bottom, from the midpoint.</small>
        <input type="number" class="form-control" aria-describedby="quilt height" placeholder="Enter Quilt Height" name="quiltheight">
        <br />

        <label for="quiltwidth">Quilt Width</label> - <small class="form-text text-muted">Make sure to use inches!</small>
        <input type="number" class="form-control" aria-describedby="quilt width" placeholder="Enter Quilt Width" name="quiltwidth">

        <br />
        <input type="submit" class="btn btn-primary btn-block" value="Calculate!" onsubmit="/borders" ></input>

        @if(isset($data))
        <div class="row">
          <div class="col col-sm-12 text-center">
            <h3 class="display-3" style="padding-bottom: 20px;">Here are your results!</h3>
            <p> Your width of fabric: {{$data['wof']}} inches </p>
            <p> Your quilt width: {{$data['quiltwidth']}}  inches </p>
            <p> Your quilt height: {{$data['quiltheight']}}  inches </p>
            <p> Your seam is {{$data['seam']}}</p>
            <strong><h3> You need  yards of fabric for your borders.</h3></strong>
            <p>Buy some backing fabric here: <a href="https://www.missouriquiltco.com/shop/browse/225?&Width=106%2522%2B%252F%2B108%2522">missouriquiltco.com</a></p>
          </div>
        </div>

        @endif

      </div>
  </div>
</form>
</div>


</div>

    </body>
</html>
