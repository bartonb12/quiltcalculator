@extends('layout.app')
    <body>
<div class="container">

  <div class="row">
    <div class="col col-md-12 text-center">
      <div class="welcome">
        <h1 class="display-1" style="font-size: 5em;">Convert Yardage to Precut</h1>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col col-md-12 text-center">
      <div class="welcome-text">
        <h3 class="display-3">Let's get started making a quilt! Choose one of the options below to start doing quilt maths!</h3>
      </div>
    </div>
  </div>

  <div class="row">
    <form>
      <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
      </div>
  </div>




</div>

    </body>
</html>
