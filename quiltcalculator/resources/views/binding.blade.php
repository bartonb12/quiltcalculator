@extends('layout.app')
    <body>
<div class="container">

  <div class="row">
    <div class="col col-md-12 text-center">
      <div class="welcome">
        <h1 class="display-1" style="font-size: 5em;">Binding Calculator</h1>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col col-md-12 text-center">
      <div class="welcome-text">
        <h3 class="display-3">Let's get started making a quilt! Choose one of the options below to start doing quilt maths!</h3>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col col-sm-4" style="padding-bottom: 5px;">
      <a href="/borders" class="btn btn-lg btn-block btn-info">Calculate Borders</a>
    </div>
    <div class="col col-sm-4" style="padding-bottom: 5px;">
      <a href="/binding" class="btn btn-lg btn-block btn-info">Calculate Binding</a>
    </div>
    <div class="col col-sm-4" style="padding-bottom: 5px;">
      <a href="/sashing" class="btn btn-lg btn-block btn-info">Calculate Sashing</a>
    </div>
  </div>

  <div class="row">
    <div class="col col-sm-4" style="padding-bottom: 5px;">
      <a href="/backing" class="btn btn-lg btn-block btn-info">Calculate Backing</a>
    </div>
    <div class="col col-sm-4" style="padding-bottom: 5px;">
      <a href="/precut-yardage" class="btn btn-lg btn-block btn-info">Precut to Yardage</a>
    </div>
    <div class="col col-sm-4" style="padding-bottom: 5px;">
      <a href="/yardage-precut" class="btn btn-lg btn-block btn-info">Yardage to Precut</a>
    </div>
  </div>

  <div class="row">
    <div class="col col-sm-4" style="padding-bottom: 5px;">

    </div>
    <div class="col col-sm-4" style="padding-bottom: 5px;">
      <a href="/pattern-scaler" class="btn btn-lg btn-block btn-info">Pattern Scaler</a>
    </div>
    <div class="col col-sm-4" style="padding-bottom: 5px;">

    </div>
  </div>


</div>

    </body>
</html>
