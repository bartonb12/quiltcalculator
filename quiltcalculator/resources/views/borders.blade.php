@extends('layout.app')
    <body>
<div class="container">

  <div class="row">
    <div class="col col-md-12 text-center">
      <div class="welcome">
        <h1 class="display-1" style="font-size: 5em;">Border Calculator</h1>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col col-md-12 text-center">
      <div class="welcome-text">
        <h3 class="display-3">How much border fabric do you need?</h3>
        <h3 style="padding-bottom: 10px;">Lets find out!</h3>
        <h4>Scroll to the bottom for results.</h4>
        <h4 style="padding-bottom: 10px;">Need something else? <a href="/">Go Back!</a></h4>
      </div>
    </div>
  </div>

  <div class="row">
    <form method="post">
      {{ csrf_field() }}
      <div class="col col-sm-12">


      <div class="form-group">
        <label for="wof">Width of Fabric</label> - <small class="form-text text-muted">43 inches is pretty standard.</small>
        <input type="number" class="form-control" aria-describedby="width of fabric" name="wof" value="43">

          <br />

        <label for="quiltheight">Quilt Height</label> - <small class="form-text text-muted">From top to bottom, from the midpoint.</small>
        <input type="number" class="form-control" aria-describedby="quilt height" placeholder="Enter Quilt Height" name="quiltheight">

          <br />

        <label for="quiltwidth">Quilt Width</label> - <small class="form-text text-muted">Make sure to use inches!</small>
        <input type="number" class="form-control" aria-describedby="quilt width" placeholder="Enter Quilt Width" name="quiltwidth">

          <br />

        <label for="borderwidth">Border Width</label> - <small class="form-text text-muted">How many inches wide do you want your borders? (Make sure to include seam allowance!)</small>
        <input type="number" step=".01" class="form-control" aria-describedby="border width" placeholder="Enter Desired Border Width" name="borderwidth">

          <br />

        <input type="submit" class="btn btn-primary btn-block" value="Calculate!" onsubmit="/borders" ></input>

        @if(isset($data))
        <div class="row">
          <div class="col col-sm-12 text-center">
            <h3 class="display-3" style="padding-bottom: 20px;">Here are your results!</h3>
            <p> Your width of fabric: {{$data['wof']}} inches </p>
            <p> Your quilt width: {{$data['quiltwidth']}} inches </p>
            <p> Your quilt height: {{$data['quiltheight']}} inches </p>
            <p> Your desired border width: {{$data['borderwidth']}} inches</p>
            <p> Your quilt's area is: {{$data['quiltarea']}} square inches</p>
            <p> The length of your border is {{$data['lengthofborder']}} inches.</p>
            <p> You need {{$data['stripsneeded']}} strips.</p>
            <strong><h3> You need {{$data['yardsneeded']}} yards of fabric for your borders.</h3></strong>
            <p>Buy some border fabric here: <a href="https://www.missouriquiltco.com/">missouriquiltco.com</a></p>
          </div>
        </div>

        @endif

      </div>
  </div>
</form>
</div>


</div>

    </body>
</html>
