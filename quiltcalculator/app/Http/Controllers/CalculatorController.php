<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CalculatorController extends Controller
{

  /**
   * Shows the Border Calculator view.
   *
   * @return view Borders calculator view.
   */
  public function borders()
  {
      return view('borders');
  }

  /**
   * Math to find quilt borders.
   *
   * @param array $request This is the data from the form.
   *
   * @return view Returns the border view with the form data and calculations.
   */
  public function bordersCalculate(Request $request)
  {
      $data                   = [];
      $wof                    = $request->input('wof');
      $quiltwidth             = $request->input('quiltwidth');
      $quiltheight            = $request->input('quiltheight');
      $borderwidth            = $request->input('borderwidth');
      $quiltarea              = $quiltheight * $quiltwidth;
      $lengthofborder         = ($quiltheight*2) + ($quiltwidth*2) + ($borderwidth*4);
      $stripsneeded           = round(($lengthofborder / $wof), 0, PHP_ROUND_HALF_UP);
      $yardsneeded            = round((($stripsneeded * $borderwidth)/36), 2, PHP_ROUND_HALF_UP);
      $data['wof']            = $wof;
      $data['quiltwidth']     = $quiltwidth;
      $data['quiltheight']    = $quiltheight;
      $data['quiltarea']      = $quiltarea;
      $data['borderwidth']    = $borderwidth;
      $data['lengthofborder'] = $lengthofborder;
      $data['stripsneeded']   = $stripsneeded;
      $data['yardsneeded']    = $yardsneeded;

      return view('borders', compact('data'));
  }

  /**
   * Shows the Backing Calculator view.
   *
   * @return view Backing calculator view.
   */
  public function backing()
  {
    return view('backing');
  }

  /**
   * Math to find quilt borders.
   *
   * @param array $request This is the data from the form.
   *
   * @return view Returns the border view with the form data and calculations.
   */
  public function backingCalculate(Request $request)
  {
      $data                = [];
      $wof                 = $request->input('wof');
      $quiltheight         = $request->input('quiltheight');
      $quiltwidth          = $request->input('quiltwidth');
      $seam                = $request->input('seam');
      $data['wof']         = $wof;
      $data['quiltheight'] = $quiltheight;
      $data['quiltwidth']  = $quiltwidth;
      $data['seam']        = $seam;

      return view('backing', compact('data'));
  }

  /**
   * Shows the Sashing Calculator view.
   *
   * @return view Sashing calculator view.
   */
  public function sashing()
  {
    return view('sashing');
  }

  /**
   * Shows the Binding Calculator view.
   *
   * @return view Binding calculator view.
   */
  public function binding()
  {
    return view('binding');
  }

  /**
   * Shows the Precut to yardage Calculator view.
   *
   * @return view Precut to yardage calculator view.
   */
  public function precutyardage()
  {
    return view('precutyardage');
  }

  /**
   * Shows the Yardage to Precut Calculator view.
   *
   * @return view Yardage to Precut calculator view.
   */
  public function yardageprecut()
  {
    return view('yardageprecut');
  }

  /**
   * Shows the Pattern Scaler view.
   *
   * @return view Pattern Scaler view.
   */
  public function patternscaler()
  {
    return view('patternscaler');
  }
}
